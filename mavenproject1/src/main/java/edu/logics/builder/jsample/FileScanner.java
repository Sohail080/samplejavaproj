/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.logics.builder.jsample;

import java.io.File;
import java.net.URL;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author awadood
 */
public class FileScanner {
    
    private static final String ROOT = "/";
    
    private static Logger log = LogManager.getLogger(FileScanner.class);
    
    public void readFiles() {
        URL url = getClass().getResource(ROOT);
        for(String fileName : new File(url.getFile()).list()) {
            log.info(fileName);
            if(fileName.endsWith("properties") || fileName.endsWith("xml")
                    || fileName.endsWith("txt") || fileName.endsWith("csv")) {
                Scanner scanner = new Scanner(getClass().getResourceAsStream(ROOT + fileName));
                while(scanner.hasNext()) {
                    log.info(scanner.nextLine());
                }
            }
        }
    }
    
}
